<?php

function config($key)
{
    $config = \App\Helpers\Config::getInstance();
    return $config->getConfig($key);
}

function logo($message, $error = false, $suffix = '')
{
    $date = date("d.m.Y/H:i:s Y/M_d");
    $date_exp = explode(" ", $date);
    $path_date = explode("/",$date_exp[1]);
    $path = PATH_DAEMON . "/logo/" . $path_date[0];
    $file = $path . "/" . $path_date[1] . $suffix .".log";

    $message = "[".$date_exp[0]."] " . $message . "\r\n";
    if(!file_exists($path))
        mkdir($path, 0777, true);

    $fp = fopen($file, "a+");
    flock($fp, LOCK_EX);
    fwrite($fp, $message);
    flock($fp, LOCK_UN); // Снятие блокировки
    fclose($fp);
    if($error)
        exit($message);

    //error_log($message, 3, PATH_DAEMON . "/logo/" . $path_date . ".log");

}