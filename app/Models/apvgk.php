<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class apvgk
 *
 * @property int $id
 * @property string $name
 * @property string $equipment_name
 * @property string $code
 * @property string $equipment_seria_number
 * @property int $equipment_type
 * @property string $owner
 * @property string $district
 * @property float $latitude
 * @property float $longitude
 * @property \Carbon\Carbon $certificate_expired_date
 * @property string $verification_number
 * @property \Carbon\Carbon $verification_date
 * @property \Carbon\Carbon $verification_expired_date
 * @property int $road_type
 * @property string $road_number
 * @property string $road_place
 * @property string $road_direction1_name
 * @property string $road_direction2_name
 * @property string $road_load
 * @property string $ftp_address
 * @property string $ftp_login
 * @property string $ftp_password
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 *
 * @package App\Models
 */

class apvgk extends Model
{
    protected $table = "apvgk";

    const SERIA_NUMBER_APVGK = "equipment_seria_number",
        FTP_ADRES = "ftp_address",
        FTP_LOGIN = "ftp_login",
        FTP_PASSWORD = "ftp_password";

    protected $fillable = [
        "name",
        "equipment_name",
        "code",
        "equipment_seria_number",
        "equipment_type",
        "owner",
        "district",
        "latitude",
        "longitude",
        "certificate_number",
        "certificate_expired_date",
        "verification_number",
        "verification_date",
        "verification_expired_date",
        "road_type",
        "road_number",
        "road_place",
        "road_direction1_name",
        "road_direction2_name",
        "road_load",
        "ftp_address",
        "ftp_login",
        "ftp_password"
    ];

    /**
     * List of data for the apvgk service
     * @var array
     */
    protected static $formatData = [
        self::SERIA_NUMBER_APVGK,
        self::FTP_ADRES,
        self::FTP_LOGIN,
        self::FTP_PASSWORD,
    ];

    /**
     * Getting data for the apvgk service
     * @return array
     */
    public static function getApvgkData()
    {
        $apvgks = [];
        $result = self::all();

        foreach ($result as $apvgk)
        {
            if($apvgk->requierFields())
            {
                $data = [];
                foreach (self::$formatData as $name) $data[$name] = $apvgk->{$name};
                $apvgks[] = $data;
            }
        }
        return $apvgks;
    }

    /**
     * Required fields
     * @return boolean
     */
    public function requierFields()
    {
        return $this->{apvgk::SERIA_NUMBER_APVGK} && $this->ftp_address;
    }

}