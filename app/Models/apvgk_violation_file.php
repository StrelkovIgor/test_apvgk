<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;



class apvgk_violation_file extends Model
{
    protected $table = "apvgk_violation_file";

    public $timestamps = false;

    public static $file_data_name = [
        "ExcessFactMedia" => "file",
        "ExcessFactMediaName" => "name",
        "ExcessFactMediaExtension" => "extension"
        ];

    protected $fillable = [
        "name",
        "id_apvgk_violation",
        "file",
        "extension"
    ];

    public static function createData($data, apvgk_violation $apvgk_violation)
    {
        foreach ($data as $file){
            $file["id_apvgk_violation"] = $apvgk_violation->id;
            self::create($file);
        }
    }

    public static function receivingFiles(&$data){
        $i = 1;
        $result = [];
        while (true){
            $dataResult = [];
            foreach (self::$file_data_name as $nameData => $name){
                $nameData .= $i;
                if(isset($data[$nameData]) && $data[$nameData]){
                    $dataResult[$name] = $data[$nameData];
                    if($nameData == "ExcessFactMedia"){
                        preg_match('/^b\'(.+)\'$/',$dataResult[$name],$preg);
                        if(count($preg))
                            $dataResult[$name] = $preg[1];
                    }
                    unset($data[$nameData]);
                }else break 2;
            }
            $result[] = $dataResult;
            $i++;
        }

        return $result;
    }

}