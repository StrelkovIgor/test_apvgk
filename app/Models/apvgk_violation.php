<?php


namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Class apvgk_violation
 * @package App\Models
 *
 * @property integer id_apvgk_data_violation
 * @property string IDBetamount
 * @property integer PlatformId
 * @property Carbon ExcessFactDate
 * @property string TrackStateNumber
 * @property string TrackCountryCode
 * @property integer TrackCategory
 * @property integer TrackSubCategory
 * @property array Properties
 * @property integer processed
 * @property integer Violation
 */
class apvgk_violation extends Model
{

    use castAttribute;

    protected $table = "apvgk_violation";

    const ID = "IDBetamount";

    protected $casts = [
        'id_apvgk_data_violation' => 'integer',
        self::ID => 'string',
        'PlatformId' => 'integer',
        'ExcessFactDate' => 'date',
        'TrackStateNumber' => 'string',
        'TrackCountryCode' => 'string',
        'TrackCategory' => 'integer',
        'TrackSubCategory' => 'integer',
        'Properties' => 'array',
        'processed' => 'integer',
        'Violation' => 'integer',
    ];

    protected $fillable = [
        "id_apvgk_data_violation",
        self::ID,
        "PlatformId",
        "ExcessFactDate",
        "TrackStateNumber",
        "TrackCountryCode",
        "TrackCategory",
        "TrackSubCategory",
        "Properties",
        "Violation"
    ];


    public static function createApvgkViolation($data, $violation = false)
    {
        DB::transaction(function() use ($data, $violation){
            $apvgkDataViolation = apvgk_data_violation::findOrCreate($data);
            $files = apvgk_violation_file::receivingFiles($data);

            $newData = [];
            $t = new self();
            foreach ($t->getFillable() as $name){
                if(isset($data[$name])){
                    $newData[$name] = $t->castAttribute($name, $data[$name]);
                    unset($data[$name]);
                }else
                    $newData[$name] = $t->castAttribute($name, '');
            }
            $apvgkViolation = self::where('IDBetamount', $newData['IDBetamount'])->first();
            if(!$apvgkViolation){
                $apvgkViolation = self::create(array_merge($newData, [
                    "id_apvgk_data_violation" => $apvgkDataViolation->id,
                    "Properties" => $data,
                ]));
                apvgk_violation_file::createData($files, $apvgkViolation);
            }

            if($violation){
                $apvgkViolation->Violation = 1;
                $apvgkViolation->save();
            }

        });
        return true;
    }
}