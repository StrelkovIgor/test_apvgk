<?php


namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class apvgk_data_violation extends Model
{
    protected $table = "apvgk_data_violation";

    public $timestamps = false;

    const NAME_FIND = "EquipmentSeriaNumber";

    protected $fillable = [
        "EquipmentName",
        "EquipmentID",
        "EquipmentType",
        "EquipmentSeriaNumber",
        "CertificateConformityNumber",
        "CertificateConformityDate",
        "CertificateStatementSuchMeasurementNumber",
        "CertificateStatementSuchMeasurementDate",
        "Place",
        "Latitude",
        "Longitude",
        "HighwayName",
        "ResolvedLoadAxisMax"
    ];

    public static function findOrCreate(&$data)
    {

        if(!(isset($data[self::NAME_FIND]) && $data[self::NAME_FIND])) throw new \Exception("The data is not present \"".self::NAME_FIND."\"");

        $obj = self::query()->where(self::NAME_FIND, $data[self::NAME_FIND])->first();
        if(!$obj){
            $obj = self::query()->newModelInstance($data);
            $obj->CertificateConformityDate = $obj->CertificateConformityDate
                ? Carbon::createFromFormat('d.m.Y', $obj->CertificateConformityDate)
                : null;
            $obj->CertificateStatementSuchMeasurementDate = $obj->CertificateStatementSuchMeasurementDate
                ? Carbon::createFromFormat('d.m.Y' , $obj->CertificateStatementSuchMeasurementDate)
                : null;
            $obj->save();
        }

        $data = array_diff_key($data, array_flip($obj->getFillable()));

        return $obj;
    }

}