<?php

namespace App\Models;


use App\Helpers\CafapServer;
use App\Helpers\FederalCafapServer;
use App\Helpers\HasCafapServer;
use Illuminate\Database\Eloquent\Model;

/**
 * Class apvgk_track_data for data from export_xmls_all
 * @package App\Models
 * @property mixed IDBetamount
 * @property mixed ExcessFactDate
 * @property mixed PlatformId
 * @property mixed TrackStateNumber
 * @property mixed TrackCountryCode
 * @property mixed TrackCategory
 * @property mixed TrackSubCategory
 * @property mixed TrackAxels
 * @property mixed MeasuredTrackWheelBase12
 * @property mixed MeasuredTrackWheelBase23
 * @property mixed MeasuredTrackWheelBase34
 * @property mixed MeasuredTrackWheelBase45
 * @property mixed TrackWheelBase12
 * @property mixed TrackWheelBase23
 * @property mixed TrackWheelBase34
 * @property mixed TrackWheelBase45
 * @property mixed MeasuredTrackGrossWeight
 * @property mixed TrackGrossWeight
 * @property mixed MeasuredTrackThrust1
 * @property mixed MeasuredTrackThrust2
 * @property mixed MeasuredTrackThrust3
 * @property mixed MeasuredTrackThrust4
 * @property mixed MeasuredTrackThrust5
 * @property mixed TrackThrust1
 * @property mixed TrackThrust2
 * @property mixed TrackThrust3
 * @property mixed TrackThrust4
 * @property mixed TrackThrust5
 * @property mixed TrackWheelsEx1
 * @property mixed TrackWheels1
 * @property mixed TrackWheelsEx2
 * @property mixed TrackWheels2
 * @property mixed TrackWheelsEx3
 * @property mixed TrackWheels3
 * @property mixed TrackWheelsEx4
 * @property mixed TrackWheels4
 * @property mixed TrackWheelsEx5
 * @property mixed TrackWheels5
 * @property mixed MeasuredTrackLength
 * @property mixed TrackLength
 * @property mixed MeasuredTrackWidth
 * @property mixed TrackWidth
 * @property mixed MeasuredTrackHeight
 * @property mixed TrackHeight
 * @property mixed Speed
 * @property mixed ExcessFactMedia1
 * @property mixed ExcessFactMediaExtension1
 * @property mixed ExcessFactMediaName1
 * @property mixed ExcessFactMedia2
 * @property mixed ExcessFactMediaExtension2
 * @property mixed ExcessFactMediaName2
 * @property mixed ExcessFactMedia3
 * @property mixed ExcessFactMediaExtension3
 * @property mixed ExcessFactMediaName3
 * @property mixed EquipmentName
 * @property mixed EquipmentID
 * @property mixed EquipmentType
 * @property mixed EquipmentSeriaNumber
 * @property mixed CertificateConformityNumber
 * @property mixed CertificateConformityDate
 * @property mixed CertificateStatementSuchMeasurementNumber
 * @property mixed CertificateStatementSuchMeasurementDate
 * @property mixed Place
 * @property mixed Latitude
 * @property mixed Longitude
 * @property mixed HighwayName
 * @property mixed ResolvedLoadAxisMax
 */
class apvgk_track_data extends Model
{
    use HasCafapServer;
    private $directory;
    private $regionNumber;
    private $licenseNumber;
    private $photoPanoramic;
    private $photoLicense;
    private $photoDetect;
    public function fill(array $attributes)
    {
        foreach ($attributes as $f => $v) {
            $this->setAttribute($f, $v);
        }
        if (!($this->cafap instanceof FederalCafapServer) && $this->cafap instanceof CafapServer) $this->checkPhotoData();
        return parent::fill($attributes);
    }

    public function getRecognisedRegionNumber()
    {
        $this->regionNumber = preg_match('/[^\d]([\d]+$)/u', $this->TrackStateNumber, $m) ? $m[1] : '';
        $this->licenseNumber = $this->regionNumber ? substr($s = $this->TrackStateNumber, 0, strpos($s, $this->regionNumber)) : '';
        return $this->regionNumber;
    }

    public function getRecognisedLicenseNumber()
    {
        return $this->licenseNumber;
    }

    private function checkPhotoData()
    {
        $this->directory = './files/cafap_photos/'. $this->EquipmentSeriaNumber .'/'.date('Y/m/d/H').'/'. $this->IDBetamount.'/';
        if (!file_exists($this->directory)) mkdir($this->directory,0777, true);
        if ($this->ExcessFactMedia1) {
            $this->photoPanoramic = $this->directory . $this->ExcessFactMediaName1 .'.'.$this->ExcessFactMediaExtension1;
            if (!file_exists($this->photoPanoramic))
                file_put_contents($this->photoPanoramic, base64_decode(substr($this->ExcessFactMedia1, 1, -1)));
        }
        if ($this->ExcessFactMedia2) {
            $this->photoDetect = $this->directory . $this->ExcessFactMediaName2 .'.'.$this->ExcessFactMediaExtension2;
            if (!file_exists($this->photoDetect))
                file_put_contents($this->photoDetect, base64_decode(substr($this->ExcessFactMedia2, 1,-1)));
        }
        if ($this->ExcessFactMedia3) {
            $this->photoLicense = $this->directory . $this->ExcessFactMediaName3 .'.'.$this->ExcessFactMediaExtension3;
            if (!file_exists($this->photoLicense))
                file_put_contents($this->photoLicense, base64_decode(substr($this->ExcessFactMedia3, 1, -1)));
        }
    }

    /**
     * @return mixed
     */
    public function getPhotoPanoramic()
    {
        return $this->photoPanoramic;
    }

    /**
     * @return mixed
     */
    public function getPhotoLicense()
    {
        return $this->photoLicense;
    }

    /**
     * @return mixed
     */
    public function getPhotoDetect()
    {
        return $this->photoDetect;
    }

    /**
     * @return mixed
     */
    public function getDirectory()
    {
        return $this->directory;
    }
}