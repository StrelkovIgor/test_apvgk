<?php

namespace App;


use App\Helpers\CafapServer;
use App\Helpers\FederalCafapServer;
use App\Helpers\HasCafapServer;
use App\Helpers\HasDictionary;
use App\Helpers\Jobs\CafapSendJob;
use App\Helpers\Jobs\Inc\WorkManager;
use App\Models\apvgk;

class CafapWorkManager extends WorkManager
{
    use HasDictionary;
    public function create()
    {
        $this->setDictionary(\App\Helpers\Dictionary::fromXls());
        $cafapApvgks = $this->getDictionary()->getApvgkList();
        $jobs = [];
        foreach (apvgk::getApvgkData() as $apvgkDB) {
            if (!isset($cafapApvgks[$apvgkDB[apvgk::SERIA_NUMBER_APVGK]])
                | (!$apvgk =$cafapApvgks[$sn = $apvgkDB[apvgk::SERIA_NUMBER_APVGK]])
                || !$apvgk['dislId (Код дислокации)']) {
                var_dump('No CAFAP data for apvgk ' . $apvgk[apvgk::SERIA_NUMBER_APVGK]);
                continue;
            }
            $job = new CafapSendJob();
            $job->setDictionary($this->getDictionary());
            $job->setCafap(new FederalCafapServer());
            $job->setEquipmentSeriaNumber($sn);
            $jobs[] = $job;
        }
        $this->setWorksheet($jobs);
    }
}