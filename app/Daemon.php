<?php


namespace App;

use App\Helpers\Jobs\Inc\WorkManager;
use App\Helpers\PcntlFork;

class Daemon
{
    /** @var WorkManager */
    private $manager;

    /** @var PcntlFork */
    private $pcntFork;

    public function __construct(WorkManager $manager)
    {
        $this->manager = $manager;
    }

    private function init()
    {
        Database::connect();
        $this->manager->create();
        if (!$this->manager->isWork())
            logo("No workflows found", true);

        Database::disconnec();

        $this->pcntFork = new PcntlFork();
        $this->pcntFork->init(
            (int)config("max_pcntl"),
            (int)config("next_process_time")
        );

    }

    public function run()
    {

        try {

            $this->init();
            $this->pcntFork->run($this->manager);
        } catch (\Exception $e) {
            $error = [
                "Message: " . $e->getMessage(),
                "File: " . $e->getFile(),
                "Code: " . $e->getCode(),
                "Line: " . $e->getLine()
            ];
            logo("[" . implode("], [", $error) . "]", true);
        }


    }


}