<?php


namespace App;


use Illuminate\Database\Capsule\Manager;

class Database
{

    private static $capsule;

    public static function connect()
    {
        $config = include PATH_DAEMON ."/config/database.php";
        self::$capsule = new Manager();
        self::$capsule->addConnection($config);
        self::$capsule->setAsGlobal();
        self::$capsule->bootEloquent();
        return self::$capsule;
    }

    public static function disconnec()
    {
        self::$capsule->getDatabaseManager()->disconnect();
    }

    public static function getManager()
    {
        return self::$capsule;
    }

}