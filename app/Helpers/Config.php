<?php


namespace App\Helpers;


class Config
{
    private static $_instance = null;
    private $_config;

	private function __construct ()
    {
        $this->_config = include PATH_DAEMON . "/config/app.php";
    }

	private function __clone () {}
	private function __wakeup () {}

	public static function getInstance()
    {
        if (self::$_instance != null) {
            return self::$_instance;
        }

        return new self;
    }

    public function getConfig($key)
    {
        return isset($this->_config[$key]) ? $this->_config[$key] : null;
    }
}