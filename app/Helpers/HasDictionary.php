<?php

namespace App\Helpers;


trait HasDictionary
{

    /** @var Dictionary */
    private $dictionary;

    /**
     * Daemon constructor.
     * @param Dictionary|null $dictionary
     */
    public function setDictionary(Dictionary $dictionary = null)
    {
        $this->dictionary = $dictionary;
    }

    /**
     * @return Dictionary
     */
    public function getDictionary(): Dictionary
    {
        return $this->dictionary;
    }
}