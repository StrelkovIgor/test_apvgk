<?php

namespace App\Helpers;


use PhpOffice\PhpSpreadsheet\Reader\Xls;

class Dictionary
{
    /** @var array */
    private $apvgk_list = [];
    /** @var array */
    private $track_categories = [];
    /** @var array */
    private $dislocations = [];
    /** @var array */
    private $sensors = [];
    /** @var array */
    private $violations = [];

    /**
     * @param string $file
     * @return self|null
     */
    public static function fromXls(string $file = '/../../apvgk_list.xls')
    {
        try {
            $reader = new Xls();
            $spreadsheet = $reader->load(__DIR__ . $file);
            $data = $spreadsheet->getSheet(0)->toArray();
            unset($data[0]);
            $dictionary = new self();
            foreach ($data[1] as $ci => $cell) {
                if($ci) $dictionary->apvgk_list[$ci] = [];
            }
            foreach ($data as $ri => $row) {
                foreach ($row as $ci => $cell) {
                    if ($ci) $dictionary->apvgk_list[$ci][($row[0] ?: 'населенный пункт')] = $cell;
                }
            }
            foreach ($dictionary->apvgk_list as $i => $apvkg) {
                $sn = $apvkg['Заводской номер'];
                if (!$sn) continue;
                $dictionary->apvgk_list[$sn] = $apvkg;
                unset($dictionary->apvgk_list[$i]);
            }
            foreach ([
                         'trackCategorySlv' => 'track_categories',
                         'dislocation' => 'dislocations',
                         'sensor' => 'sensors',
                         'violationSubType' => 'violations',
                     ] as $sheetname => $field) {
                $data = $spreadsheet->getSheetByName($sheetname)->toArray();
                $heads = $data[0];
                unset($data[0]);
                foreach ($data as $ri => $row) {
                    if (array_filter($row)) $dictionary->$field[] = array_combine($heads, $row);
                }
            }
            return $dictionary;
        }catch (\Throwable $e) {
            var_dump($e->getTraceAsString());
            return null;
        }

    }

    /**
     * @return array
     */
    public function getApvgkList(): array
    {
        return $this->apvgk_list;
    }

    /**
     * @param array $apvgk_list
     */
    public function setApvgkList(array $apvgk_list): void
    {
        $this->apvgk_list = $apvgk_list;
    }

    /**
     * @return array
     */
    public function getTrackCategories(): array
    {
        return $this->track_categories;
    }

    /**
     * @param array $track_categories
     */
    public function setTrackCategories(array $track_categories): void
    {
        $this->track_categories = $track_categories;
    }

    /**
     * @return array
     */
    public function getDislocations(): array
    {
        return $this->dislocations;
    }

    /**
     * @param array $dislocations
     */
    public function setDislocations(array $dislocations): void
    {
        $this->dislocations = $dislocations;
    }

    /**
     * @return array
     */
    public function getSensors(): array
    {
        return $this->sensors;
    }

    /**
     * @param array $sensors
     */
    public function setSensors(array $sensors): void
    {
        $this->sensors = $sensors;
    }

    /**
     * @return array
     */
    public function getViolations(): array
    {
        return $this->violations;
    }

    /**
     * @param array $violations
     */
    public function setViolations(array $violations): void
    {
        $this->violations = $violations;
    }
}