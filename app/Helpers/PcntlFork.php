<?php
declare(ticks=1);

namespace App\Helpers;

use App\Helpers\Jobs\Inc\JobInterface;
use App\Helpers\Jobs\Inc\WorkManager;

class PcntlFork
{
    // Максимальное количество дочерних процессов
    public $maxProcesses = 1;
    // Когда установится в TRUE, демон завершит работу
    protected $stop_server = FALSE;
    // Здесь будем хранить запущенные дочерние процессы
    protected $childs = array();

    /**
     * WorkManager
     * @var \App\Helpers\Jobs\Inc\WorkManager
     */
    private $manager;

    private $sleep_maxProcesses = 10;

    public function __construct()
    {
        if (!function_exists('pcntl_fork')) die('PCNTL functions not available on this PHP installation');

        $this->init();

        echo "Сonstructed daemon controller" . PHP_EOL;
        // Ждем сигналы SIGTERM и SIGCHLD
        pcntl_signal(SIGTERM, array($this, "childSignalHandler"));
        pcntl_signal(SIGCHLD, array($this, "childSignalHandler"));
        pcntl_signal(SIGHUP, array($this, "childSignalHandler"));
//        pcntl_signal(SIGKILL, array($this, "childSignalHandler"));
        pcntl_signal_dispatch();
    }

    /**
     * @param int $maxProcesses
     * @param int $sleep_maxProcesses
     */
    public function init(int $maxProcesses = 1, int $sleep_maxProcesses = 10)
    {
        $this->maxProcesses = $maxProcesses <= $this->maxProcesses || $maxProcesses >= 50 ? $this->maxProcesses : $maxProcesses;
        $this->sleep_maxProcesses = $sleep_maxProcesses;
    }

    /**
     * @param WorkManager $manager
     * @return bool
     */
    public function run(WorkManager $manager)
    {
        $this->manager = $manager;
        $this->agreementMaxProcesses();

        for ($i = 0; !$this->stop_server; $i++) {
            if (!$this->stop_server and (count($this->childs) < $this->maxProcesses)) {
                $pid = pcntl_fork();
                if ($pid == -1) {
                    logo('Could not launch new job, exiting');
                    return FALSE;
                } elseif ($pid) {
                    //процесс создан
                    $this->childs[$pid] = $i;
                } else {
                    $manager->doTheJob($i, $this->childs);
                    exit;
                }
            } else {
//                var_dump("Сон " . $this->sleep_maxProcesses);
                sleep($this->sleep_maxProcesses);
            }

            while ($signaled_pid = pcntl_waitpid(-1, $status, WNOHANG)) {
                if ($signaled_pid == -1) {
                    //детей не осталось
                    $this->childs = array();
                    break;
                } else {
                    unset($this->childs[$signaled_pid]);
                }
            }
            sleep(10);
        }
    }

    public function childSignalHandler($signo, $pid = null, $status = null)
    {
        if (is_array($pid) && isset($pid['pid']))
            $pid = $pid['pid'];

        switch ($signo) {
            case SIGKILL:
            case SIGHUP:
            case SIGTERM:
                // При получении сигнала завершения работы устанавливаем флаг
                logo("Daemon termination signal received");
                var_dump("Daemon stop");
                $this->stop_server = true;
                break;
            case SIGCHLD:
                // При получении сигнала от дочернего процесса
                if (!$pid) {
                    $pid = pcntl_waitpid(-1, $status, WNOHANG);
                }
                // Пока есть завершенные дочерние процессы
                while ($pid > 0) {
                    if ($pid && isset($this->childs[$pid])) {
                        // Удаляем дочерние процессы из списка

                        unset($this->childs[$pid]);
                    }
                    $pid = pcntl_waitpid(-1, $status, WNOHANG);
                }
                break;
            default:
                // все остальные сигналы
        }
    }

    public function stoped()
    {
        $this->stop_server = true;
    }

    private function agreementMaxProcesses()
    {
        $worksheet = count($this->manager->getWorksheet());
        $this->maxProcesses = $worksheet < $this->maxProcesses ? $worksheet : $this->maxProcesses;
    }

}