<?php

namespace App\Helpers;


trait HasCafapServer
{
    /**
     * @var CafapServer|FederalCafapServer
     */
    private $cafap;

    /**
     * @return CafapServer
     */
    public function getCafap(): CafapServer
    {
        return $this->cafap;
    }

    /**
     * @param CafapServer $cafap
     */
    public function setCafap(CafapServer $cafap): void
    {
        $this->cafap = $cafap;
    }

}