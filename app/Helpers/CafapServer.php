<?php
namespace App\Helpers;
use App\Models\apvgk_track_data;

/**
 * Class Cafap
 * @package App\Helpers
 */

class CafapServer
{
    private $sessionId;
    private $eventId;
    public const FILES_DATA_PATH = 'cafap';
    public const CAFAP_URL = 'http://0.0.0.0:4000';
    public const CONNECT_URI = '/cafap-api/connect';
    public const TRACK_DATA_URI = '/cafap-api/addevent';
    public const ADD_DATA_URI = '/cafap-api/addpicture';
    public const ASSURANCE_DEFAULT = 95;

    public function connect(array $sensorData)
    {
        try {
            $response = $this->sendRequest(self::CONNECT_URI, array_merge($sensorData, [
                'cafapToken' => config('cafap_token'),
                'userTxt' => 'AISKTG',
            ]));
            $this->sessionId = json_decode($response, true)['sessionId'];
            if (!$this->sessionId) {
                var_dump($sensorData);
                var_dump($response);
            }
        } catch (\Throwable $e) {
            logo($e->getMessage());
        }
        return (bool) $this->sessionId;
    }
    public function sendTrackData(apvgk_track_data $track_data, array $apvgk, Dictionary $d) {
        try {
            $response = $this->sendRequest(self::TRACK_DATA_URI, array_merge([
                'eventDateTime' => $track_data->ExcessFactDate,
                'movementDirection' => $apvgk[$track_data->PlatformId],
                'fixedSpeed' => $track_data->Speed,
                'recognisedRegionNumber' => $track_data->getRecognisedRegionNumber() ,
                'recognisedLicenseNumber' => $track_data->getRecognisedLicenseNumber(),
                'assuranceValue' => self::ASSURANCE_DEFAULT,
                'channelNum' => '1',
                'xLU' => '1',
                'yLU' => '1',
                'xRD' => '10',
                'yRD' => '10',
                'latitude' => $track_data->Latitude,
                'longitude' => $track_data->Longitude,
                'photoPanoramic' => new \CURLFile($track_data->getPhotoPanoramic(), 'image/jpeg'),
                'sessionId' => $this->sessionId
            ], $track_data->getPhotoLicense() ? [
                'photoLicensePlate' => new \CURLFile($track_data->getPhotoLicense(), 'image/jpeg'),
            ] : []));
            if (!json_decode($response)['error']) return -1; //не нужно пыттаьтся отправит повторно
            logo($response);
            if (($this->eventId = json_decode($response, true)['eventId'])
                && $track_data->getPhotoDetect()) {
                $response = $this->sendRequest(self::ADD_DATA_URI,array_merge([
                    'xLU' => '1',
                    'yLU' => '1',
                    'xRD' => '10',
                    'yRD' => '10',
                    'picture' => new \CURLFile($track_data->getPhotoDetect(), 'image/jpeg'),
                    'eventId' => $this->eventId
                ]));
                logo($response);
            }
        } catch (\Throwable $e) {
            logo($e->getMessage());
        }
        if ($track_data && file_exists($track_data->getDirectory())) {
            shell_exec('rm -rf ' .  $track_data->getDirectory());
        }
        return $this->eventId;
    }

    /**
     * @param string $url
     * @param array $data
     * @return string
     */
    protected function sendRequest(string $url, array $data) : string
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => self::CAFAP_URL . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

}