<?php

namespace App\Helpers\Soap\Cafap;


class process
{
    /** @var message $message */
    public $message;

    /**
     * process constructor.
     * @param message $message
     */
    public function __construct(message $message)
    {
        $this->message = $message;
    }

}