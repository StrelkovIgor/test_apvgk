<?php

namespace App\Helpers\Soap\Cafap;


use App\Models\apvgk_track_data;

class photoExtra
{
    /** @var dateTime $v_frame_datetime*/
    public $v_frame_datetime;
    /** @var base64|string $v_photo_extra*/
    public $v_photo_extra;
    /** @var string $v_type_photo */
    public $v_type_photo;

    /**
     * photoExtra constructor.
     * @param dateTime $v_frame_datetime
     * @param base64|string|string $v_photo_extra
     * @param string $v_type_photo
     */
    public function __construct(apvgk_track_data $data)
    {
        $this->v_frame_datetime = null;
        $this->v_photo_extra = $data->ExcessFactMedia2;
        $this->v_type_photo = 'a'; //тип из документации, "а" - для печати в постановление
    }
}