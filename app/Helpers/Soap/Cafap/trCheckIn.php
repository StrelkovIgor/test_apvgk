<?php


namespace App\Helpers\Soap\Cafap;


use App\Models\apvgk_track_data;

class trCheckIn
{
    /*@var dateTime $v_time_check*/
    public $v_time_check;
    /*@var string $v_camera*/
    public $v_camera;
    /*@var double $v_gps_x*/
    public $v_gps_x;
    /*@var double $v_gps_y*/
    public $v_gps_y;
    /*@var double $v_azimut*/
    public $v_azimut;
    /*@var string $v_direction*/
    public $v_direction;
    /*@var int $v_speed_limit*/
    public $v_speed_limit;
    /*@var double $v_speed*/
    public $v_speed;
    /*@var string $v_regno_country_id*/
    public $v_regno_country_id;
    /*@var int $v_regno_color_id*/
    public $v_regno_color_id;
    /*@var double $v_recognition_accuracy*/
    public $v_recognition_accuracy;
    /*@var string $v_regno*/
    public $v_regno;
    /*@var int $v_pr_viol*/
    public $v_pr_viol;
    /*@var int $v_parking_num*/
    public $v_parking_num;
    /*@var int $v_parking_zone*/
    public $v_parking_zone;
    /*@var int $v_lane_num*/
    public $v_lane_num;
    /*@var string $v_camera_place*/
    public $v_camera_place;
    /*@var base64Binary $v_photo_grz*/
    public $v_photo_grz;
    /*@var base64Binary $v_photo_ts*/
    public $v_photo_ts;

    /**
     * trCheckIn constructor.
     * @param apvgk_track_data $data
     */
    public function __construct(apvgk_track_data $data)
    {
        $this->v_time_check = str_replace(' ', 'T', $data->ExcessFactDate);
        $this->v_camera = $data->EquipmentSeriaNumber;
        $this->v_gps_x = $data->Longitude;
        $this->v_gps_y = $data->Latitude;
        $this->v_azimut = null;
//        $this->v_direction = $data->PlatformId; //после уточнения, совпадает ли "от датчика" с имеющимся "из города"
        $this->v_speed_limit = null;
        $this->v_speed = $data->Speed;
        $this->v_regno_country_id = $data->TrackCountryCode;
        $this->v_regno_color_id = null;
        $this->v_recognition_accuracy = null;
        $this->v_regno = $data->TrackStateNumber;
        $this->v_pr_viol = null;
        $this->v_parking_num = null;
        $this->v_parking_zone = null;
        $this->v_lane_num = null;
        $this->v_camera_place = $data->Place;
        $this->v_photo_grz = $data->ExcessFactMedia3;
        $this->v_photo_ts = $data->ExcessFactMedia1;
    }
}