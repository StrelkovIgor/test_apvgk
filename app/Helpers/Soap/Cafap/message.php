<?php

namespace App\Helpers\Soap\Cafap;


class message
{
    public $tr_checkIn;
    public $photo_extra;

    /**
     * message constructor.
     * @param $tr_checkIn
     * @param $photo_extra
     */
    public function __construct($tr_checkIn, $photo_extra = null)
    {
        $this->tr_checkIn = $tr_checkIn;
        $this->photo_extra = $photo_extra;
    }
}