<?php


namespace App\Helpers\Jobs;


use App\Database;
use App\Helpers\CafapServer;
use App\Helpers\Jobs\Inc\apvgkServer;
use App\Helpers\Jobs\Inc\Job;
use App\Helpers\Jobs\Inc\JobInterface;
use App\Models\apvgk_violation;

class ApvgkFilesJob extends Job implements JobInterface
{

    private $apvgkServer = null;

    private $filesLimit = 100;
    private $delete = false;
    private $paths = null;
    private $path_copy;
    private $cafap_path = CafapServer::FILES_DATA_PATH;

    public function __construct(apvgkServer $apvgkServer)
    {
        $this->apvgkServer = $apvgkServer;
        $this->init();
    }

    private function init()
    {
        $this->filesLimit = (int) config("ftp_files_processing");
        $this->delete = (boolean) config("ftp_deleting_processed_files");
        $this->paths = config("paths");
        $this->pathCopy();
    }

    private function pathCopy()
    {

        foreach ($this->paths['path_ftp'] + [$this->cafap_path => null] as $key => $value){

            $pathCopu = explode("/",$this->paths['path_save_files']);
            array_push($pathCopu, $key);
            array_push($pathCopu, $this->apvgkServer->getName());
            $pathCopu = implode("/", $pathCopu);
            if ($key === $this->cafap_path) {
                $this->cafap_path  = $pathCopu;
            } else $this->path_copy[$key] = $pathCopu;

            if(!file_exists($pathCopu)){
                mkdir($pathCopu, 0777, true);
            }
        }
    }

    public function perform()
    {
        Database::connect();
        $this->apvgkServer->connect();
//        var_dump('connected to' . $this->apvgkServer->getName() . $this->apvgkServer->getIp());
        if($this->apvgkServer->availability())
        {
            $cafap_files = [];
            foreach ($this->paths['path_ftp'] as $key => $path)
            {
                $ftp_list_files = $this->apvgkServer->getListFile($path,$this->filesLimit);
                $size = count($ftp_list_files);
                for ($i = 0; $i < $size && (!$this->filesLimit || $i < $this->filesLimit); $i++){
                    $file = $ftp_list_files[$size - $i - 1];

                    $localFile = $this->path_copy[$key]. str_replace('./'.$key, '', $file);
                    if (strpos($path, $this->paths['cafap_ftp_path']) !== false && !file_exists($localFile)) {
                        $cafap_files[] = $localFile;
                    }
                    if (strpos($to = $this->path_copy[$key], ($tmp = $this->paths['path_tmp']) .'/')) {
                        $tmpMoveDir = str_replace($tmp, $this->paths['path_ftp_violation'], $to);
                        if (file_exists($tmpMoveDir .'/'. pathinfo($file)['basename'])) continue;
                    }
                    $isCopy = $this->apvgkServer->copyFile($to, $file);

//                    Опять меняем локиу, берем только нарушение. Эту логику оставляем на всякий случай
//                    if($isCopy && $key == 'export_xmls_gruz')
//                        $isCopy = $this->recordDb($to, $file);

                    if($isCopy && $this->delete)
                        $this->apvgkServer->delete($file);
                }
            }
            if ($cafap_files) file_put_contents($this->cafap_path . '/' . time() . '.json', json_encode($cafap_files));
        }
//        var_dump('files list end');
        $this->apvgkServer->disconnect();
        Database::disconnec();

    }

    public function toString()
    {
        return parent::toString() .$this->apvgkServer->getName() . '('.$this->apvgkServer->getIp().')';
    }

    private function recordDb($to, $file)
    {
        try {
            $info = pathinfo($file);
            $file = $to . "/". $info['basename'];
            if (!file_exists($file) && $info['extension'] === "xml") return false;
            $transfer_file = true;
            $xml = simplexml_load_file($file);
            foreach ((array)$xml->item->attributes() as $data)
                if (!apvgk_violation::createApvgkViolation($data)){
                    logo("2_File that cannot be written: " . $file);
                    $transfer_file = false;
                }
        } catch (\Throwable $e) {
            logo($e->getMessage());
            logo('Bad violation xml file2 ' . $file);
//                    unlink($file);
            return false;
        }
        return $transfer_file;
    }


}