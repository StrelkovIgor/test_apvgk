<?php
namespace App\Helpers\Jobs;


use App\Helpers\CafapServer;
use App\Helpers\HasCafapServer;
use App\Helpers\HasDictionary;
use App\Helpers\Jobs\Inc\Job;
use App\Models\apvgk_track_data;

class CafapSendJob extends Job
{
    use HasCafapServer, HasDictionary;
    private $equipment_seria_number;
    public function perform()
    {
        $suf = 'cafap_from_' . $this->equipment_seria_number;
        $path = config('paths')['path_save_files'] . '/' . CafapServer::FILES_DATA_PATH .'/' . $this->equipment_seria_number . '/';
        foreach ($this->getFiles($path) as $jsonFile) {
            if (pathinfo($jsonFile)['extension'] == 'json') {
                logo("Cafap connect with " . $this->equipment_seria_number, false, $suf);
                $state = $this->cafap->connect([
                    'dislId' => ($d = $this->getDictionary()
                        ->getApvgkList())[$this->equipment_seria_number]['dislId (Код дислокации)'],
                    'sensorName' => $d[$this->equipment_seria_number]['Идентификатор датчика(sensorName)'],
                ]);
                logo("Cafap {$this->equipment_seria_number} " .($state ? 'connected' : 'not connected'), false, $suf);
                if (!$state) return;
                $filesToSend = json_decode(file_get_contents($jsonFile), true) ?: [];
                foreach ($filesToSend as $i => $file) {
                    try {
                        $send = false;
                        if (file_exists($file)) foreach ((array) simplexml_load_file($file)->item->attributes() as $j => $data){
                            $track_data = apvgk_track_data::query()->newModelInstance();
                            $track_data->setCafap($this->cafap);
                            $track_data->forceFill($data);
                            $send |= (bool) $this->cafap->sendTrackData($track_data, $this->getDictionary()->getApvgkList()[$this->equipment_seria_number], $this->getDictionary());
                            if ($send) logo('Sended '.($i+1) .' files ' . $file, false, $suf);
                        }
                        if ($send) unset($filesToSend[$i]);
                    } catch (\Throwable $e) {
                        logo($e->getTraceAsString(), false, $suf);
                        logo('Bad xml file' . $file, $suf);
                    }
                }
                if ($filesToSend) {
                    file_put_contents($jsonFile, json_encode(array_values($filesToSend)));
                    rename($jsonFile, str_replace($r = pathinfo($jsonFile)['basename'], time() .'_bad.json', $jsonFile));
                } else {
                    unlink($jsonFile);
                }
            }
        }

    }

    /**
     * @return mixed
     */
    public function getEquipmentSeriaNumber()
    {
        return $this->equipment_seria_number;
    }

    /**
     * @param mixed $equipment_seria_number
     */
    public function setEquipmentSeriaNumber($equipment_seria_number): void
    {
        $this->equipment_seria_number = $equipment_seria_number;
    }
    public function toString()
    {
        return parent::toString() .'('. $this->equipment_seria_number .')';
    }
}