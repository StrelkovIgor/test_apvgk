<?php


namespace App\Helpers\Jobs\Inc;


class ApvgkServer
{

    private $nameAPVGK = null;
    private $ip, $login, $password, $port;

    private $connect, $enter;

    /**
     * default
     */
    private $_login, $_password, $_port;

    public function __construct($nameAPVGK, $ip, $login = null, $password = null, $port = null)
    {
        $this->nameAPVGK = $nameAPVGK;
        $this->ip = $ip;
        $this->login = $login;
        $this->password = $password;
        $this->port = $port;

        $this->setDefault();
    }

    private function setDefault()
    {
        $this->_login = config("ftp_user_default");
        $this->_password = config("ftp_password_default");
        $this->_port = config("ftp_port_default");
    }

    public function isCheck()
    {
        $this->connect();
        if($this->isConnect())
            $this->disconnect();
        return $this->availability();

    }

    public function connect()
    {
        $this->connect =  @ftp_connect($this->ip,$this->getPort(),20);
        if(!$this->connect) logo("FTP connection problem. Name: \"".$this->getName()."\"");

        if($this->isConnect())
        {
            $this->enter = ftp_login($this->connect , $this->getLogin(), $this->getPassword());
            if(!$this->enter) logo("FTP Authorization failed. Name: \"".$this->getName()."\"");

            ftp_set_option($this->connect, FTP_USEPASVADDRESS, false); // set ftp option
            ftp_pasv($this->connect, true); //make connection to passive mode
        }

    }

    public function isConnect()
    {
        return $this->connect !== false;
    }

    public function availability()
    {
        return ($this->connect !== false && $this->enter);
    }

    public function disconnect()
    {
        ftp_close($this->connect);
    }

    public function getName()
    {
        $name = preg_replace("/[^a-z0-9_-]/i", "", $this->nameAPVGK);
        $name = $name == "" ? $this->ip : $name ;
        return mb_strtolower($name, 'UTF-8');
    }

    private function getLogin()
    {
        return $this->login ?? $this->_login;
    }

    private function getPassword()
    {
        return $this->password ?? $this->_password;
    }

    private function getPort()
    {
        return $this->port ?? $this->_port;
    }

    public function getListFile($path)
    {
        try {
            return ftp_nlist($this->connect, $path);
        }catch (\Exception $e){
            logo("Can't get a list of files from the server: \"" . $this->getName() . "\"");
            logo($e->getTraceAsString(), true);
        }
    }

    public function copyFile($locale_path, $server_path)
    {
        $info = pathinfo($server_path);
        if (file_exists($locale_path . "/" . $info['basename'])) return true;
        $stream = fopen($locale_path . "/". $info['basename'],"w");
        $result = ftp_fget($this->connect, $stream,$server_path,FTP_BINARY, 0);
        fclose($stream);

        return $result;
    }

    public function delete($server_path)
    {
        return ftp_delete($this->connect, $server_path);
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }


}