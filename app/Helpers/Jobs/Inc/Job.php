<?php


namespace App\Helpers\Jobs\Inc;


use App\Helpers\HasCafapServer;

abstract class Job implements JobInterface
{
    use HasCafapServer;
    protected $sleep = 10;

    public abstract function perform();

    public function execution()
    {
//        var_dump('Job started');
        $this->perform();
//        var_dump('Job work ended');

        sleep($this->sleep);

        return true;
    }

    public function isDone()
    {
        return $this->execution === false;
    }


    protected function getFiles($path)
    {
        $listFile = [];
        $dir = opendir($path);
        while($file = readdir($dir)){
            $path_file = $path . "/" . $file;
            if ($file != '.' && $file != '..'){
                if(is_dir($path_file))
                    $listFile = array_merge($listFile , $this->getFiles($path_file));
                if(is_file($path_file)){
                    array_push($listFile, $path_file);
                }
            }
        }
        return $listFile;
    }
    public function toString()
    {
        return class_basename($this);
    }


}