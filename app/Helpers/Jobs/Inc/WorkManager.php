<?php


namespace App\Helpers\Jobs\Inc;


use App\Helpers\Jobs\AddingViolationJob;
use App\Helpers\Jobs\ApvgkFilesJob;
use App\Models\apvgk;

class WorkManager
{
    /**
     * Work queue
     * @var array|JobInterface[]
     */
    private $worksheet = [];

    /**
     * Work counter
     * @var int
     */
    private $position = 0;


    /**
     * Create a queue of jobs
     * @return void
     */
    public function create()
    {
        $this->worksheet = $this->getJobsApvgk();
        $this->getJobsAddingViolation();
    }

    /**
     * get a list of works apvgk
     * @return array|ApvgkFilesJob[]
     */
    public function getJobsApvgk()
    {
        $data = [];
        $apvgks = apvgk::getApvgkData();
        foreach ($apvgks as $apvgk)
        {
            $server = new ApvgkServer(
                $apvgk[apvgk::SERIA_NUMBER_APVGK],
                $apvgk[apvgk::FTP_ADRES],
                $apvgk[apvgk::FTP_LOGIN],
                $apvgk[apvgk::FTP_PASSWORD]
            );
            if($server->isCheck()) {
                $data[] = new ApvgkFilesJob($server);
            }else
                logo("During the initialization phase, it was not possible to connect to the server. Server name \"".$server->getName()."({$server->getIp()})\"");
        }

        return $data;
    }

    public function getJobsAddingViolation()
    {
        $start = false;
        foreach ($this->getWorksheet() as $job)
            if($job instanceof ApvgkFilesJob){
                $start = true; break;
            }
        if($start)
            $this->setWorksheet(new AddingViolationJob());
        else
            logo("Work on processing files will not start because there are no working servers");
    }

    /**
     * @return null|JobInterface
     */
    private function getJob()
    {
        if($this->position >= count($this->worksheet))
            $this->position = 0;

        $position = $this->position;
        $this->position++;

        return  isset($this->worksheet[$position]) ? $this->worksheet[$position] : null;
    }

    /**
     * @return bool
     */
    public function isWork()
    {
        return (boolean) count($this->worksheet);
    }

    /**
     * @return JobInterface[]|array
     */
    public function getWorksheet()
    {
        return $this->worksheet;
    }

    /**
     * @param JobInterface[]|JobInterface $data
     */
    public function setWorksheet($data)
    {
        if ($data && !is_array($data)) $data = [$data];
        if (!$data) return;
        foreach ($data as $work)
            if($work instanceof JobInterface)
                $this->worksheet[] = $work;
    }

    private function clarificationProcess($processList, $numberJob)
    {
        if(!count($processList)) return false;

        foreach ($processList as $process)
            if($process[1] === $numberJob) return false;

        return true;
    }

    public function getNumberJob($inc)
    {
        $worksheet = $this->getWorksheet();
        $jobCount = count($worksheet);
        return $inc % $jobCount;
    }



    public function doTheJob($i, $processList){
//        var_dump(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//        var_dump($processList);
//        var_dump("Счетчик " . $inc);

        $worksheet = $this->getWorksheet();
        $jobCount = count($worksheet);
        $i = $i % $jobCount;
        $job = $worksheet[$i];

//        var_dump("Количество работ " . $jobCount);
//        var_dump("Номер работы " . $numberJob);

        $j = 0;
        foreach ($processList as $pid => $processNumber)
            if($processNumber % $jobCount === $i){
//                var_dump("Совпадение " . $processNumber);
                $j = $pid;
                break;
            }

//        var_dump("Сделать работу " . (!$j ? "Да" : "Нет"));

        if(($job instanceof JobInterface) && !$j){
//            var_dump($job->toString());
            $job->execution();
//            var_dump("Процесс ". $i. " выполнен. Работа ". $i);
//            var_dump("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        }
        exit;
    }



}