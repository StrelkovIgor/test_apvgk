<?php


namespace App\Helpers\Jobs\Inc;


interface JobInterface
{
    public function perform();
    public function execution();
    public function toString();

}