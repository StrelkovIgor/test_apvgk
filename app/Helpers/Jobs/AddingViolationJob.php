<?php


namespace App\Helpers\Jobs;


use App\Database;
use App\Helpers\Jobs\Inc\Job;
use App\Helpers\Jobs\Inc\JobInterface;
use App\Models\apvgk_violation;

class AddingViolationJob extends Job implements JobInterface
{

    protected $sleep = 300;

    private $paths = null;
    private $newPath = null;

    public function __construct()
    {
        $this->paths = config("paths");

        $this->newPath = $this->paths['path_save_files'] . "/" . $this->paths['path_ftp_violation'];
        $this->createDir($this->newPath);
    }

    public function perform()
    {
//        var_dump("Начало работы");
//        sleep(rand(1,50));
//        var_dump("Работа выполнена!!!");
        Database::connect();
        $path = $this->paths['path_save_files'] . "/" . $this->paths['path_tmp'];

        foreach($this->getFiles($path) as $file){
            $transfer_file = true;
            $info = pathinfo($file);
            if($info['extension'] === "xml"){
                try {
                    $xml = simplexml_load_file($file);
                    foreach((array) $xml->item->attributes() as $data)
                        if(!$this->saveData($data)){
                            logo("File that cannot be written: " . $file);
                            $transfer_file = false;
                        }

                    if($transfer_file){
                        $dir = explode("/", $info['dirname']);
                        $dir_transfer = $this->newPath . "/" . $dir[count($dir) - 1];
                        $this->createDir($dir_transfer);
                        rename($file, $dir_transfer . "/" . $info['basename']);
                    }
                } catch (\Throwable $e) {
//                    Часто бывает что пытается сжувать файл которые еще загружается, не нужно его удалять
//                    logo($e->getMessage());
//                    logo('Bad violation xml file ' . $file);
//                    unlink($file);
                }
            }
        }

        Database::disconnec();
    }

    private function saveData(array $data)
    {
        try {
            return apvgk_violation::createApvgkViolation($data,true);
        }catch (\Exception $e){
            logo(class_basename($e) );
            logo($e->getMessage());
//            logo($e->getTraceAsString());
            return false;
        }
        return true;
    }

    private function createDir($path)
    {
        if(!file_exists($path)){
            mkdir($path, 0777, true);
        }
    }

    public function toString()
    {
        return parent::toString() .$this->apvgkServer->getName() . '('.$this->apvgkServer->getIp().')';
    }

}