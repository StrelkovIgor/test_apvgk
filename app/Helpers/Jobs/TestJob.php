<?php


namespace App\Helpers\Jobs;


use App\Helpers\Jobs\Inc\apvgkServer;
use App\Helpers\Jobs\Inc\Job;
use App\Helpers\Jobs\Inc\JobInterface;

class TestJob extends Job implements JobInterface
{
    public $rand;


    public function perform()
    {
        // TODO: Implement perform() method.
        $this->rand = rand(200,500);
        var_dump($this->rand);
        \sleep($this->rand);

    }

    public function __toString()
    {
        return parent::toString() . '('.$this->rand.')';
    }

}