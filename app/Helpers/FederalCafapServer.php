<?php

namespace App\Helpers;

use App\Helpers\Soap\Cafap\message;
use App\Helpers\Soap\Cafap\photoExtra;
use App\Helpers\Soap\Cafap\process;
use App\Helpers\Soap\Cafap\trCheckIn;
use App\Models\apvgk_track_data;

/**
 * Class FederalCafapServer
 * @package App\Helpers
 */
class FederalCafapServer extends CafapServer
{
    public const CAFAP_DEFINITION = 'https://web-duplo2.gibdd.ru/duplo/services/duplo2?wsdl';
    public const CAFAP_NORESEND_STATUS = 'TRAFFIC-ERROR';
    private static $client;

    public function connect(array $sensorData)
    {
        try {
            if (!self::$client)
                self::$client = new \SoapClient(self::CAFAP_DEFINITION, [
                    'soap_version' => SOAP_1_2,
                    'connection_timeout' => 5000,
                ]);
            return true;
        } catch (\Throwable $e) {
            logo($e->getMessage());
        }
        return false;
    }

    public function sendTrackData(apvgk_track_data $track_data, array $apvgk, Dictionary $d)
    {
        try {
            $tr = new trCheckIn($track_data);
            $process = new process($m = new message($tr));
            $m->photo_extra = new photoExtra($track_data);
            $response = self::$client->process($process);
            return $response->return;
        } catch (\SoapFault $e) {
//            var_dump($e->detail);
            logo($e->faultstring);
            if ($e->detail) logo("Soap response: {$e->detail->DuploFault->faultCode} {$e->detail->DuploFault->faultMessage}", false, 'soap_faults');
            return $e->faultstring === self::CAFAP_NORESEND_STATUS;
        } catch (\Throwable $e) {
            logo($e->getTraceAsString());
            logo($e->getMessage());
        }
        return false;
    }


}