<?php
return [
    //ftp
    //Пользователь по умолчанию, если в базе не найдет пароль
    "ftp_user_default" => env("USER_DEFAULT","frameb7"),
    //Пароль по умолчанию, если в базе не найдет пароль
    "ftp_password_default" => env("PASSWORD_DEFAULT","frameb7_export"),
    //Порт по умолчанию, если в базе не указан порт
    "ftp_port_default" => env("PORT_DEFAULT", 21),
    //Количество файлов будут обработаны в каждой директории "path_ftp" за 1 процесс (pcntl). Если указать 0 то будут обработаны все файлы за 1 процесс
    "ftp_files_processing" => env("FILES_PROCESSING", 100),
    //Удаление файлов после копирования
    //Если установленно false и ftp_files_processing = 100 то процесс будет копировать одни и теже файлы так как процесс не запоменает на каком файле он остановился
    "ftp_deleting_processed_files" => env("DELETE_FILES", false),

    //Директории
    //Как будет создваться путь: path_save_files + path_ftp(Локальный путь) + Имя АПВГК (Имя берестся из базы gibdd.apvgk.code)

    "paths" => [
        //Куда будут копироваться файлы, можно использовать как относительный путь так и обсалютный
        "path_save_files" => "./files",
        //Правило пути (Локальный путь) => (Путь на ftp)
        "path_ftp" => [
            "export_xmls_tmp" =>"./export_xmls",
            "export_xmls_all" =>"./export_xmls_all",
            "export_xmls_gruz" =>"./export_xmls_gruz"
            ],
        //Временая дириктория, откуда будет брать файлы для парсинга и записи в базу. После обработки файл будет перемещен в "path_ftp_violation"
        "path_tmp" => "export_xmls_tmp",
        //Сюда будут копироватся файлы после обработки (path_tmp)
        "path_ftp_violation" => "export_xmls",
        //дирректория данные  из которой отправлятся в ЦАФАП
        "cafap_ftp_path" => "export_xmls_all",
    ],

    //pcntl - многопроцессность
//    "pcntl" => true,        //true - многопроцессность, false - 1 процесс
    "max_pcntl" => env("MAX_PCNTL",5),       //Максимальное количество процесов
    "next_process_time" => env("NEXT_PROCESS_TIME", 10), //Время ожидание следующего процесса
    "cafap_token" => env("CAFAP_TOKEN"),

];