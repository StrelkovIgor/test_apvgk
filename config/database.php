<?php

return [
    "driver" => "mysql",
    "host" => env('DB_HOST','mysql'),
    "database" => env('DB_DATABASE','gibdd'),
    "username" => env('DB_USERNAME','root2'),
    "password" => env('DB_PASSWORD','root2'),
    "charset" => "utf8",
    "collation" => "utf8_unicode_ci",
    "prefix" => "",
];
