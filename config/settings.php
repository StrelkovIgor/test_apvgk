<?php
//declare(ticks=1); // Используется в классе с многозадачностью
set_time_limit(0);
error_reporting(E_ALL & ~E_NOTICE);
ini_set("memory_limit","-1");

ini_set('display_errors', 0);

set_error_handler("errorAll", E_ALL^E_WARNING);

function errorAll($errno, $errmsg = "Null", $filename ="Null", $linenum ="Null", $vars = "Null"){
        $error = [
            "Message: " . $errno,
            "File: " . $filename,
            "Code: " . $errmsg,
            "Line: " . $linenum
        ];
        logo("[" . implode("], [", $error) . "]",true);
}
set_error_handler(function($errno, $errstr, $errfile, $errline) {
    // error was suppressed with the @-operator
    if (0 === error_reporting()) {
        return false;
    }

    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}, E_WARNING);


function shutdown() {
    $error = error_get_last();
    if($error)
        errorAll($error['message'],"null", $error['file'], $error['line']);
}
register_shutdown_function('shutdown');