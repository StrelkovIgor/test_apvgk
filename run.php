<?php
define("PATH_DAEMON", __DIR__);
if (! function_exists('pcntl_fork')) die('PCNTL functions not available on this PHP installation');
/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/

//composer dump-autoload -o


require PATH_DAEMON . "/config/settings.php";

require PATH_DAEMON . '/vendor/autoload.php';

//Global function
require  PATH_DAEMON . "/app/Global_function.php";

$manager = $argv && isset($argv[1]) && $argv[1] === 'cafap'
    ? new \App\CafapWorkManager()
    : new \App\Helpers\Jobs\Inc\WorkManager();
$daemon = new \App\Daemon($manager);
$daemon->run();
